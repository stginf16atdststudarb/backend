#!/usr/bin/env python3

import logging
import os

import connexion
from flask_cors import CORS

from api.metrics import afterRequest, beforeRequest
from api.models.databaseORM import db

logger = logging.getLogger(__name__)


app = connexion.App(__name__, specification_dir='openapi/')
app.add_api('openapi.yml')

# Set CORS headers
CORS(app.app)

# Register request latency trackers
app.app.before_request(beforeRequest)
app.app.after_request(afterRequest)

# set the WSGI application callable to allow using uWSGI:
# uwsgi --http :8080 -w app
application = app.app

logger.info('App initialized')
application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

if 'DATABASE_URL' in os.environ:
    # Cloud Foundry runtime
    DBURL = os.environ['DATABASE_URL']

elif 'CI' in os.environ:
    # Environment for pytest during gitlab ci run
    DBURL = f'postgresql+psycopg2://{os.environ["POSTGRES_USER"]}:{os.environ["POSTGRES_PASSWORD"]}@postgres/{os.environ["POSTGRES_DB"]}'

else:
    # Local development environment
    from secrets.testCredentials import DATABASE
    DBURL = DATABASE

application.config['SQLALCHEMY_DATABASE_URI'] = DBURL


with application.app_context():
    db.init_app(app.app)
    db.create_all()

logger.info('Database Initialized')
