#!/usr/bin/env python3
"""Gunicorn configuration
"""


def worker_exit(server, worker):
    from prometheus_client import multiprocess
    multiprocess.mark_process_dead(worker.pid)
