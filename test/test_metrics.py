#!/usr/bin/env python3

import pytest

from .TestConnexion import TestConnexion


@pytest.mark.usefixtures('client')
class TestMetrics(TestConnexion):
    """Tests metrics endpoint
    """
    def test_metrics(self, client):
        response = client.get('/api/v1/metrics')
        assert response.status_code == 200
