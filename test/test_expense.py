import uuid
from datetime import datetime, timedelta
import pytest

from .TestConnexion import TestConnexion


@pytest.mark.usefixtures('client', 'admin', 'pedestrian', 'unauthUser')
class TestExpense(TestConnexion):
    """Test expense creation and deletion
    """

    @pytest.fixture(scope='class')
    def sharedGroup(self, client, admin, pedestrian):
        """Create a group to use in other tests
        """
        body = {
            "groupName": "Holiday Trip",
            "nickname": "Charlie Chaplin"
        }
        response = client.post('/api/v1/group', json=body, headers=self.generateHeaders(admin, body))
        assert response.status_code == 200

        groupID = response.get_json()

        expiryTime = datetime.now() + timedelta(days=7)
        body = {
            'expiryTime': expiryTime.isoformat()
        }

        response = client.post(f'/api/v1/group/{groupID}/invite', json=body, headers=self.generateHeaders(admin, body))
        assert response.status_code == 200

        inviteToken = response.get_json()
        assert inviteToken is not None

        body = {
            'nickname': 'Anton Hynkel'
        }
        response = client.post(f'/api/v1/group/invite/{inviteToken}', json=body, headers=self.generateHeaders(pedestrian, body))
        assert response.status_code == 200
        assert response.get_json() == groupID

        try:
            yield groupID
        finally:
            # Delete the group
            response = client.delete(f'/api/v1/group/{groupID}', headers=self.generateHeaders(admin, ''))
            assert response.status_code == 204

    @pytest.fixture()
    def expense(self, client, admin, pedestrian, unauthUser, sharedGroup):
        body = {
            "description": "Money for coffee",
            "positions": [
                {
                    "amount": 3.14,
                    "userID": admin['userID']
                },
                {
                    "amount": 3.14,
                    "userID": pedestrian['userID']
                }
            ],
            'date': datetime.now().isoformat()
        }

        apiPath = f'/api/v1/group/{sharedGroup}/expense'

        # Test with a user who is not member of the group
        response = client.post(apiPath, json=body, headers=self.generateHeaders(unauthUser, body))
        assert response.status_code == 403

        # Now with admin
        response = client.post(apiPath, json=body, headers=self.generateHeaders(admin, body))

        assert response.status_code == 200
        expenseID = response.get_json()

        yield expenseID

        # Teardown --> delete expense
        apiPath = f'/api/v1/group/{sharedGroup}/expense/{expenseID}'
        # Not a member
        response = client.delete(apiPath, headers=self.generateHeaders(unauthUser, ''))
        assert response.status_code == 403

        # Insufficient permissions
        response = client.delete(apiPath, headers=self.generateHeaders(pedestrian, ''))
        assert response.status_code == 403

        # Should delete
        response = client.delete(apiPath, headers=self.generateHeaders(admin, ''))
        assert response.status_code == 204

    def test_getExpenses(self, client, admin, unauthUser, sharedGroup, expense):
        apiPath = f'/api/v1/group/{sharedGroup}/expense'

        # Not a member
        response = client.get(apiPath, headers=self.generateHeaders(unauthUser, ''))
        assert response.status_code == 403

        response = client.get(apiPath, headers=self.generateHeaders(admin, ''))
        assert response.status_code == 200

        expenses = response.get_json()
        assert expenses[0]['expenseID'] == expense
        assert expenses[0]['amount'] == 6.28
        assert expenses[0]['confirmed'] is False

    def test_getExpense(self, client, admin, unauthUser, sharedGroup, expense):
        # Expense which does not exist
        apiPath = f'/api/v1/group/{sharedGroup}/expense/{uuid.uuid4()}'
        response = client.get(apiPath, headers=self.generateHeaders(admin, ''))
        assert response.status_code == 404

        # Now a real expense
        apiPath = f'/api/v1/group/{sharedGroup}/expense/{expense}'

        # Not a member
        response = client.get(apiPath, headers=self.generateHeaders(unauthUser, ''))
        assert response.status_code == 403

        response = client.get(apiPath, headers=self.generateHeaders(admin, ''))
        assert response.status_code == 200

        expenseData = response.get_json()
        assert expenseData['expenseID'] == expense
        assert expenseData['positions'][0]['amount'] == 3.14

    def test_getBalance(self, client, admin, sharedGroup, expense):
        response = client.get(f'/api/v1/group/{sharedGroup}/balance', headers=self.generateHeaders(admin, ''))

        assert response.status_code == 200

    def test_confirmExpense(self, client, pedestrian, unauthUser, sharedGroup, expense):
        apiPath = f'/api/v1/group/{sharedGroup}/expense/{expense}/confirm'
        # Not a member
        # response = client.get(apiPath, headers=self.generateHeaders(unauthUser, ''))
        # assert response.status_code == 403

        repsonse = client.post(apiPath, headers=self.generateHeaders(pedestrian, ''))
        assert repsonse.status_code == 204

    def test_updateExpense(self, client, admin, sharedGroup, expense):
        apiPath = f'/api/v1/group/{sharedGroup}/expense/{expense}'
        body = {
            "description": "Money for tea",
            'date': datetime.now().isoformat()
        }

        repsonse = client.put(apiPath, json=body, headers=self.generateHeaders(admin, body))
        assert repsonse.status_code == 204

    def test_add_update_deletePosition(self, client, admin, sharedGroup, expense):
        apiPath = f'/api/v1/group/{sharedGroup}/expense/{expense}/position'
        body = {
            'amount': 6.28,
            'userID': admin['userID']
        }
        response = client.post(apiPath, json=body, headers=self.generateHeaders(admin, body))
        assert response.status_code == 204

        # Try to get a positionID
        apiPath = f'/api/v1/group/{sharedGroup}/expense/{expense}'
        response = client.get(apiPath, headers=self.generateHeaders(admin, ''))
        assert response.status_code == 200

        expenseData = response.get_json()
        # Check a position has been added
        assert len(expenseData['positions']) == 3

        # Now save the position id to update and delete it
        positionID = expenseData['positions'][0]['positionID']
        user = expenseData['positions'][0]['userID']

        # Now update that position
        apiPath = f'/api/v1/group/{sharedGroup}/expense/{expense}/position/{positionID}'
        body = {
            'userID': user,
            'amount': 1.57
        }
        repsonse = client.put(apiPath, json=body, headers=self.generateHeaders(admin, body))
        assert repsonse.status_code == 204

        # And now delete
        repsonse = client.delete(apiPath, headers=self.generateHeaders(admin, ''))
        assert repsonse.status_code == 204

    def test_lockGroup(self, client, admin, unauthUser, sharedGroup, expense):
        # Not a member
        response = client.post(f'/api/v1/group/{sharedGroup}/lock', headers=self.generateHeaders(unauthUser, ''))
        assert response.status_code == 404

        # Should succeed
        response = client.post(f'/api/v1/group/{sharedGroup}/lock', headers=self.generateHeaders(admin, ''))
        assert response.status_code == 204

        # Check whether group is really locked
        response = client.get(f'/api/v1/group/{sharedGroup}', headers=self.generateHeaders(admin, ''))
        assert response.status_code == 200

        groupData = response.get_json()
        assert groupData['locked']

    def test_getPayments(self, client, admin, unauthUser, sharedGroup, expense):
        response = client.get(f'/api/v1/group/{sharedGroup}/payment', headers=self.generateHeaders(admin, ''))
        assert response.status_code == 200
