#!/usr/bin/env python3

import uuid
from datetime import datetime, timedelta
import pytest

from .TestConnexion import TestConnexion


@pytest.mark.usefixtures('client', 'admin', 'pedestrian', 'unauthUser', 'group')
class TestGroup(TestConnexion):
    """Tests basic user creation
    """
    @pytest.mark.dependency(name='createUser')
    def test_getUserGroupsEmpty(self, client, pedestrian):
        response = client.get('/api/v1/user/groups', headers=self.generateHeaders(pedestrian, ''))
        assert response.status_code == 200

    def test_getGroup(self, client, admin, unauthUser, group):
        response = client.get(f'/api/v1/group/{group}', headers=self.generateHeaders(admin, ''))
        assert response.status_code == 200

        groupData = response.get_json()
        assert groupData['groupID'] == group

        # Not a member
        response = client.get(f'/api/v1/group/{group}', headers=self.generateHeaders(unauthUser, ''))
        assert response.status_code == 404

    def test_getUserGroups(self, client, admin, group):
        response = client.get('/api/v1/user/groups', headers=self.generateHeaders(admin, ''))
        assert response.status_code == 200

        body = response.get_json()
        assert body[0]['groupID'] == group

    @pytest.mark.dependency(name='createInvite')
    def test_createInvite(self, client, admin, pedestrian, group):
        expiryTime = datetime.now() + timedelta(days=7)
        body = {
            'expiryTime': expiryTime.isoformat()
        }

        # Group which doesn't exist
        apiPath = f'/api/v1/group/{uuid.uuid4()}/invite'
        response = client.post(apiPath, json=body, headers=self.generateHeaders(admin, body))
        assert response.status_code == 404

        apiPath = f'/api/v1/group/{group}/invite'

        # Insufficient privileges
        response = client.post(apiPath, json=body, headers=self.generateHeaders(pedestrian, body))
        assert response.status_code == 404

        response = client.post(apiPath, json=body, headers=self.generateHeaders(admin, body))
        assert response.status_code == 200

        self.cache['inviteToken'] = response.get_json()

        assert self.cache['inviteToken'] is not None

    @pytest.mark.dependency(name='groupJoin', depends=['createInvite', 'createUser'])
    def test_groupJoin(self, client, pedestrian, group):
        apiPath = f'/api/v1/group/invite/{uuid.uuid4()}'

        body = {
            'nickname': 'Anton Hynkel'
        }

        # Test an invented token
        response = client.post(apiPath, json=body, headers=self.generateHeaders(pedestrian, body))
        assert response.status_code == 404

        apiPath = f'/api/v1/group/invite/{self.cache["inviteToken"]}'
        response = client.post(apiPath, json=body, headers=self.generateHeaders(pedestrian, body))
        assert response.status_code == 200
        assert response.get_json() == group

    @pytest.mark.dependency(name='getUserGroupsMember', depends=['groupJoin'])
    def test_getUserGroupsMember(self, client, pedestrian, group):
        response = client.get('/api/v1/user/groups', headers=self.generateHeaders(pedestrian, ''))
        assert response.status_code == 200

        body = response.get_json()
        assert body[0]['groupID'] == group

    @pytest.mark.dependency(name='deleteGroup', depends=['createUser', 'groupJoin'])
    def test_deleteGroupInsufficientPrivileges(self, client, pedestrian, unauthUser, group):
        apiPath = f'/api/v1/group/{uuid.uuid4()}'
        # Group doesn't exist
        response = client.delete(apiPath, headers=self.generateHeaders(pedestrian, ''))
        assert response.status_code == 404

        apiPath = f'/api/v1/group/{group}'
        # Not a member
        response = client.delete(apiPath, headers=self.generateHeaders(unauthUser, ''))
        assert response.status_code == 404

        # Try to delete as non-privileged user
        response = client.delete(apiPath, headers=self.generateHeaders(pedestrian, ''))
        assert response.status_code == 403

    @pytest.mark.dependency(name='promoteUser', depends=['createUser', 'groupJoin', 'deleteGroup'])
    def test_promoteUser(self, client, admin, pedestrian, group):
        apiPath = f'/api/v1/group/{group}/member/promote'

        response = client.post(apiPath, json=pedestrian['userID'], headers=self.generateHeaders(admin, f'"{pedestrian["userID"]}"'))
        assert response.status_code == 204
