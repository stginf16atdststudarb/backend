
import base64
import logging
import random
import struct
from string import Template
from hashlib import sha384
from fastecdsa import curve, ecdsa, keys
from fastecdsa.encoding.pem import PEMEncoder

logger = logging.getLogger(__name__)


def test_create_and_verify_signature():
    privateKey = keys.gen_private_key(curve.P384)
    publicKey = keys.get_public_key(privateKey, curve.P384)
    pemKey = PEMEncoder.encode_public_key(publicKey)

    # Strip the headers and create a single line string of the pubkey
    spkiKey = ''.join(pemKey.split('\n')[1:-1])

    logger.debug('pemKey: %s', pemKey)
    logger.debug('spkiKey: %s', spkiKey)

    rand_float = random.SystemRandom().random()
    salt = base64.b64encode((struct.pack('!d', rand_float)))
    logger.debug('Salt: %s', salt)

    message = base64.b64decode(salt) + ''.encode('utf-8')
    logger.debug('Message: %s', message)

    r, s = ecdsa.sign(message, privateKey, curve=curve.P384, hashfunc=sha384)
    logger.debug('r: %d', r)
    logger.debug('s: %d', s)

    assert ecdsa.verify((r, s), message, publicKey, hashfunc=sha384, curve=curve.P384)

    r = r.to_bytes(48, byteorder='big')
    s = s.to_bytes(48, byteorder='big')

    sig = base64.b64encode(b''.join([r, s]))
    logger.debug('Sig: %s', sig)

    # Build the auth token and start to validate
    authToken = f'{salt.decode("utf-8")}.{sig.decode("utf-8")}.{spkiKey}'
    logger.debug('AuthToken: %s', authToken)

    salt2, userSignature, userPubkey = authToken.split('.')
    pemHeader = '-----BEGIN PUBLIC KEY-----'
    pemFooter = '-----END PUBLIC KEY-----'

    pemKey = Template('$header\n$pubkey\n$footer').substitute(header=pemHeader, footer=pemFooter, pubkey=userPubkey)
    pubKey = PEMEncoder.decode_public_key(pemKey, curve=curve.P384)

    sig = bytearray(base64.b64decode(userSignature))
    r = int.from_bytes(sig[:48], byteorder='big', signed=False)
    s = int.from_bytes(sig[48:], byteorder='big', signed=False)

    logger.debug('r: %d', r)
    logger.debug('s: %d', s)

    message2 = base64.b64decode(salt2) + b''
    logger.debug('Message to verify: %s', message2)

    assert message == message2

    assert ecdsa.verify((r, s), message2, pubKey, hashfunc=sha384, curve=curve.P384)
