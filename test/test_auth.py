import pytest

from .TestConnexion import TestConnexion


@pytest.mark.usefixtures('client', 'admin')
class TestAuth(TestConnexion):
    """Tests authentication
    """
    def test_authValid(self, client, admin):
        response = client.get('/api/v1/user', headers=self.generateHeaders(admin, ''))
        assert response.status_code == 200

        assert response.get_json() == admin['userID']

    def test_authInvalid(self, client, admin):
        response = client.get('/api/v1/user', headers=self.generateHeaders(admin, {'Nothing': 'go ahead'}))
        assert response.status_code == 401

    def test_authInvalidToken(self, client):
        header = {
            'Authorization': 'Bearer a.b.c'
        }
        response = client.get('/api/v1/user', headers=header)
        assert response.status_code == 401

    def test_authNoToken(self, client):
        response = client.get('/api/v1/user')
        assert response.status_code == 401

    def test_authWrongToken(self, client):
        header = {
            'Authorization': 'Bearer abc'
        }
        response = client.get('/api/v1/user', headers=header)
        assert response.status_code == 401
