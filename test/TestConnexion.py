#!/usr/bin/env python3

import base64
import random
import struct
import json

from hashlib import sha384
from fastecdsa import curve, ecdsa, keys
from fastecdsa.encoding.pem import PEMEncoder

import pytest

from app import app


class TestConnexion:
    """The base test providing auth and flask clients to other tests
    """
    cache: dict = {}

    @pytest.fixture(scope='session')
    def client(self):
        with app.app.test_client() as c:
            yield c

    @pytest.fixture(scope='session')
    def admin(self, client):
        """Generates a user to be used as admin
        """

        privateKey = keys.gen_private_key(curve.P384)
        publicKey = keys.get_public_key(privateKey, curve.P384)
        pemKey = PEMEncoder.encode_public_key(publicKey)
        spkiKey = ''.join(pemKey.split('\n')[1:-1])

        response = client.post('/api/v1/user/register', json={'userKey': spkiKey})
        assert response.status_code == 200

        return {
            'privateKey': privateKey,
            'publicKey': publicKey,
            'pemKey': pemKey,
            'spkiKey': spkiKey,
            'userID': response.get_json()
        }

    @pytest.fixture(scope='session')
    def pedestrian(self, client):
        """Generates a user to be used as pedestrian
        """
        privateKey = keys.gen_private_key(curve.P384)
        publicKey = keys.get_public_key(privateKey, curve.P384)
        pemKey = PEMEncoder.encode_public_key(publicKey)
        spkiKey = ''.join(pemKey.split('\n')[1:-1])

        response = client.post('/api/v1/user/register', json={'userKey': spkiKey})
        assert response.status_code == 200

        return {
            'privateKey': privateKey,
            'publicKey': publicKey,
            'pemKey': pemKey,
            'spkiKey': spkiKey,
            'userID': response.get_json()
        }

    @pytest.fixture(scope='session')
    def unauthUser(self, client):
        """Generates a user to be used as a non-member
        """
        privateKey = keys.gen_private_key(curve.P384)
        publicKey = keys.get_public_key(privateKey, curve.P384)
        pemKey = PEMEncoder.encode_public_key(publicKey)
        spkiKey = ''.join(pemKey.split('\n')[1:-1])

        response = client.post('/api/v1/user/register', json={'userKey': spkiKey})
        assert response.status_code == 200

        return {
            'privateKey': privateKey,
            'publicKey': publicKey,
            'pemKey': pemKey,
            'spkiKey': spkiKey,
            'userID': response.get_json()
        }

    @pytest.fixture(scope='class')
    def group(self, client, admin):
        """Create a group to use in other tests
        """
        body = {
            "groupName": "Holiday Trip",
            "nickname": "Charlie Chaplin"
        }
        response = client.post('/api/v1/group', json=body, headers=self.generateHeaders(admin, body))
        assert response.status_code == 200

        groupID = response.get_json()

        try:
            yield groupID
        finally:
            # Delete the group
            response = client.delete(f'/api/v1/group/{groupID}', headers=self.generateHeaders(admin, ''))
            assert response.status_code == 204

    def generateKeys(self, user):
        """Generates the required private and public key
        """
        self.cache[user]['privateKey'] = keys.gen_private_key(curve.P384)
        self.cache[user]['publicKey'] = keys.get_public_key(self.cache[user]['privateKey'], curve.P384)
        self.cache[user]['pemKey'] = PEMEncoder.encode_public_key(self.cache[user]['publicKey'])

        # Strip the headers and create a single line string of the pubkey
        self.cache[user]['spkiKey'] = ''.join(self.cache[user]['pemKey'].split('\n')[1:-1])

    def generateHeaders(self, user, body):
        """Generates a signed authentication bearer token
        """
        if isinstance(body, dict):
            body = json.dumps(body, sort_keys=True)

        rand_float = random.SystemRandom().random()
        salt = base64.b64encode((struct.pack('!d', rand_float)))
        message = base64.b64decode(salt) + body.encode('utf-8')

        r, s = ecdsa.sign(message, user['privateKey'], curve=curve.P384, hashfunc=sha384)
        r = r.to_bytes(48, byteorder='big')
        s = s.to_bytes(48, byteorder='big')

        sig = base64.b64encode(b''.join([r, s]))

        spkiKey = user['spkiKey']

        return {
            'Authorization': f'Bearer {salt.decode("utf-8")}.{sig.decode("utf-8")}.{spkiKey}'
        }
