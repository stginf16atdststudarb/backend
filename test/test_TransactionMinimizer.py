#!/usr/bin/env python3

import itertools
import logging
import random
import uuid
from datetime import datetime

import pytest

from api.models.TransactionMinimizer import TransactionMinimizer

logger = logging.getLogger(__name__)


class TestTransactionMinimizer:
    @pytest.fixture(scope='class')
    def expenses(self):
        users = [str(uuid.uuid4()) for _ in itertools.repeat(None, random.randint(2, 20))]
        # users = [f'User{letter}' for letter in ['A', 'B', 'C', 'D']]
        expenseList = list()
        for _ in itertools.repeat(None, random.randint(1, 30)):
            positions = list()

            for _ in itertools.repeat(None, random.randint(2, 30)):
                positions.append({
                    "amount": random.randint(0, 200000),
                    "userID": random.choice(users)
                })

            expenseList.append({
                "creator": random.choice(users),
                "positions": positions,
                'date': datetime.now().isoformat()
            })
        return expenseList

    def test_generateBalanceGraph(self, expenses):
        users = {}  # Absue dict as ordered set
        balance = {}
        logger.debug('Constructing debt dict')
        # Generate a dict with all others have by a user
        for expense in expenses:
            logger.debug('Creator: %s', expense['creator'])
            users[expense['creator']] = None
            balance.setdefault(expense['creator'], {})
            for position in expense['positions']:
                users[position['userID']] = None
                logger.debug('Defaulter: %s', position['userID'])
                balance[expense['creator']].setdefault(position['userID'], 0)
                balance[expense['creator']][position['userID']] += position['amount']
        logger.debug('Balance (unmerged): %s', balance)
        logger.debug('Members: %s', users)

        rows = list()
        # Build the matrix
        for user in users:
            userBalance = balance.setdefault(user, {})
            column = list()
            for innerUser in users:
                if innerUser == user:
                    column.append(0)
                else:
                    column.append(userBalance.setdefault(innerUser, 0))
            rows.append(column)

        tm = TransactionMinimizer(rows, list(users))

        payments = tm.minCashFlow()
        logger.debug(payments)
