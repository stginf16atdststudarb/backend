#!/usr/bin/env python3
"""Functions related to managing payments
"""
import logging

import api.models.database as db
from api.models.APIError import APIError
from api.models.customTypes import Action

logger = logging.getLogger(__name__)


def getPayments(user, groupID):
    """ Get payments for this group
    """
    group = db.getGroup(groupID)

    if group is None or not group.isMember(user):
        return APIError.newError(404, 'Unknown group')

    if not group.locked:
        return APIError.newError(409, 'Group is not locked')

    payments = [payment.serialize for payment in db.getPayments(groupID)]

    return payments, 200


def updatePayment(user, groupID, paymentID, action):
    """Set a payment to be confirmed or received
    """
    group = db.getGroup(groupID)
    if group is None or not group.isMember(user):
        return APIError.newError(404, 'Unknown group')

    payment = db.getPayment(paymentID)

    if payment.groupID != group.groupID:
        return APIError.newError(404, 'Payment not found')

    if action == Action.SENT and payment.debtor == user:
        db.markPayment(payment, Action.SENT)
        return '', 204

    if action == Action.REC and payment.recipient == user:
        db.markPayment(payment, Action.REC)
        return '', 204

    return APIError(403, 'Wrong side of heaven')
