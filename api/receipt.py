#!/usr/bin/env python3
"""Functions related to managing receipts
"""
import io
import json
import logging
import mimetypes
import os
import uuid
from string import Template

import ibm_boto3
from connexion import request
from flask import send_file
from ibm_botocore.client import ClientError, Config

import api.models.database as db
from api.models.APIError import APIError

logger = logging.getLogger(__name__)


COS_ENDPOINT = 'https://s3.eu-de.cloud-object-storage.appdomain.cloud'
COS_AUTH_ENDPOINT = 'https://iam.cloud.ibm.com/identity/token'

if 'VCAP_SERVICES' in os.environ:
    VCAP = json.loads(os.environ['VCAP_SERVICES'])
    credentials = VCAP['cloud-object-storage'][0]['credentials']
    COS_API_KEY_ID = credentials['apikey']
    COS_RESOURCE_CRN = credentials['resource_instance_id']


def getReceipt(user, groupID, expenseID, receiptID):
    if 'VCAP_SERVICES' not in os.environ:
        return APIError.newError(451, 'This deployment does not support attachments')
    group = db.getGroup(groupID)

    if group is None or not group.isMember(user):
        return APIError.newError(404, 'Unknown group')

    expense = db.getExpense(expenseID)

    if expense is None or expense.groupID != group.groupID:
        return APIError.newError(404, 'Unknown expense')

    cos = ibm_boto3.resource("s3", ibm_api_key_id=COS_API_KEY_ID, ibm_service_instance_id=COS_RESOURCE_CRN, ibm_auth_endpoint=COS_AUTH_ENDPOINT, config=Config(signature_version="oauth"), endpoint_url=COS_ENDPOINT)

    try:
        attachment = cos.Object('reisekosten', receiptID).get()
    except ClientError as be:
        logger.warning(be)
        return APIError.newError(503, str(be))

    # Generate a file name with ending from mimetype
    name = Template('$name.$extension')
    name = name.substitute(name=receiptID, extension=mimetypes.guess_extension(attachment['ContentType']))

    return send_file(
        io.BytesIO(attachment['Body'].read()),
        mimetype=attachment['ContentType'],
        attachment_filename=name), 200


def uploadReceipt(user, groupID, expenseID, body):
    if 'VCAP_SERVICES' not in os.environ:
        return APIError.newError(451, 'This deployment does not support attachments')
    group = db.getGroup(groupID)

    if group is None or not group.isMember(user):
        return APIError.newError(404, 'Unknown group')

    expense = db.getExpense(expenseID)

    if expense is None or expense.groupID != group.groupID:
        return APIError.newError(404, 'Unknown expense')

    cos = ibm_boto3.resource("s3", ibm_api_key_id=COS_API_KEY_ID, ibm_service_instance_id=COS_RESOURCE_CRN, ibm_auth_endpoint=COS_AUTH_ENDPOINT, config=Config(signature_version="oauth"), endpoint_url=COS_ENDPOINT)

    fileID = str(uuid.uuid4())

    try:
        cos.Object('reisekosten', fileID).put(Body=body, ContentType=request.headers.get('Content-Type'))
    except ClientError as be:
        logger.warning(be)
        return APIError.newError(503, str(be))

    db.createAttachment(expenseID, fileID)

    return fileID, 200
