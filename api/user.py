#!/usr/bin/env python3
"""Functions related to managing users
"""
import logging

import api.models.database as db

logger = logging.getLogger(__name__)


def getUserID(user):
    """Returns the users ID
    """
    return user, 200


def register(body):
    """Adds a key (a new device) to  new user object
    """
    logger.debug('New user with key %s', body['userKey'])
    userID = db.createUser(body['userKey'])
    logger.debug('Created user %s', userID)

    # Persist now
    return userID, 200


def getUserGroups(user):
    """Returns a list of groups a user is a member of

    Arguments:
        user {UserID} -- UUID of the requesting user
    """
    usr = db.getUser(user)
    return usr.getGroups(), 200


def registerPush():
    pass
