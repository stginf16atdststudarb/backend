#!/usr/bin/env python3
"""Implementation of a greedy algorithm to minimize the number of transactions

Also see https://www.geeksforgeeks.org/minimize-cash-flow-among-given-set-friends-borrowed-money/
"""

from typing import List
from prometheus_client import Histogram
from .customTypes import UserID


class TransactionMinimizer:
    """Implements an minimizer to get
    the minimal amount of transactions
    utilizing a greedy algorithm.
    """
    RKB_TRANSMIN_LATENCY = Histogram('rkb_transaction_minimizer_latency_seconds', 'Time consumed by the minimization process')

    def __init__(self, graph: List[List[int]], persons: List[UserID]):
        self.graph = graph
        self.persons = persons
        self.dimension = len(persons)
        self.payments: List[dict] = list()

    def getMin(self, arr: List[List[int]]) -> int:
        """A utility function that returns
        index of minimum value in arr[]

        Arguments:
            arr {List[List[int]]} -- [description]

        Returns:
            int -- [description]
        """
        minInd = 0
        for i in range(1, self.dimension):
            if arr[i] < arr[minInd]:
                minInd = i
        return minInd

    def getMax(self, arr: List[List[int]]) -> int:
        """A utility function that returns
        index of maximum value in arr[]

        Arguments:
            arr {List[List[int]]} -- [description]

        Returns:
            int -- [description]
        """
        maxInd = 0
        for i in range(1, self.dimension):
            if arr[i] > arr[maxInd]:
                maxInd = i
        return maxInd

    @staticmethod
    def minOf2(x: int, y: int) -> int:
        """A utility function to
        return minimum of 2 values

        Arguments:
            x {int} -- [description]
            y {int} -- [description]

        Returns:
            int -- [description]
        """
        return x if x < y else y

    def minCashFlowRec(self, amount: List[List[int]]) -> int:
        """
        amount[p] indicates the net amount to
        be credited/debited to/from person 'p'
        If amount[p] is positive, then i'th
        person will amount[i]
        If amount[p] is negative, then i'th
        person will give -amount[i]

        Arguments:
            amount {List[List[int]]} -- [description]

        Returns:
            int -- [description]
        """
        # Find the indexes of minimum
        # and maximum values in amount[]
        # amount[mxCredit] indicates the maximum
        # amount to be given(or credited) to any person.
        # And amount[mxDebit] indicates the maximum amount
        # to be taken (or debited) from any person.
        # So if there is a positive value in amount[],
        # then there must be a negative value
        mxCredit = self.getMax(amount)
        mxDebit = self.getMin(amount)

        # If both amounts are 0,
        # then all amounts are settled
        if (amount[mxCredit] == 0 and amount[mxDebit] == 0):
            return

        # Find the minimum of two amounts
        minimum = self.minOf2(-amount[mxDebit], amount[mxCredit])
        amount[mxCredit] -= minimum
        amount[mxDebit] += minimum

        # If minimum is the maximum amount
        payment = {
            'recipient': self.persons[mxDebit],  # Dunno, seems wrong
            'debtor': self.persons[mxCredit],
            'amount': minimum / 100
        }
        self.payments.append(payment)

        # Recurse for the amount array. Note that
        # it is guaranteed that the recursion
        # would terminate as either amount[mxCredit]
        # or amount[mxDebit] becomes 0
        self.minCashFlowRec(amount)

    @RKB_TRANSMIN_LATENCY.time()
    def minCashFlow(self):
        """
        Given a set of persons as graph[] where
        graph[i][j] indicates the amount that
        person i needs to pay person j, this
        function finds and prints the minimum
        cash flow to settle all debts.
        """

        # Create an array amount[],
        # initialize all value in it as 0.
        amount = [0 for i in range(self.dimension)]

        # Calculate the net amount to be paid
        # to person 'p', and stores it in amount[p].
        # The value of amount[p] can be calculated by
        # subtracting debts of 'p' from credits of 'p'
        for p in range(self.dimension):
            for i in range(self.dimension):
                amount[p] += (self.graph[i][p] - self.graph[p][i])

        self.minCashFlowRec(amount)

        return self.payments
