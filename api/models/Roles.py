#!/usr/bin/env python3
"""This module defines a Role
"""

from enum import Enum


class Role(str, Enum):
    """Roles of a member in a group (determines permissions)
    """
    ADMIN = 'admin'
    PED = 'pedestrian'
