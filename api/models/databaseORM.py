#!/usr/bin/env python3
"""This module keeps the database orm mapping
"""
import logging
import time
from typing import List, Tuple

from flask_sqlalchemy import SQLAlchemy
from prometheus_client import Histogram
from sqlalchemy import event
from sqlalchemy.dialects.postgresql import UUID as saUUID
from sqlalchemy.engine import Engine
from sqlalchemy.sql import func

from api.models.customTypes import UserID
from api.models.Roles import Role

logger = logging.getLogger(__name__)

RKB_DATABASE_LATENCY = Histogram('rkb_database_latency_seconds', 'Time consumed for each database query', ['query_type'])
RKB_GRAPH_GENERATION_LATENCY = Histogram('rkb_graph_generation_latency_seconds', 'Time consumed to generate the final graph', ['group_id'])

db = SQLAlchemy()
session = db.session


@event.listens_for(Engine, "before_cursor_execute")
def before_cursor_execute(conn, cursor, statement, parameters, context, executemany):  # pylint: disable=unused-argument, too-many-arguments
    conn.info.setdefault('query_start_time', []).append(time.time())
    logger.debug("Start Query: %s", statement)


@event.listens_for(Engine, "after_cursor_execute")
def after_cursor_execute(conn, cursor, statement, parameters, context, executemany):  # pylint: disable=unused-argument, too-many-arguments
    total = time.time() - conn.info['query_start_time'].pop(-1)
    queryType = statement.strip().split(' ')[0]
    logger.debug("%s Query Complete!", queryType)
    logger.debug("Total Time: %f", total)

    RKB_DATABASE_LATENCY.labels(queryType.lower()).observe(total)


class DBUserGroups(db.Model):
    """The n:m relationship between a user and a group with extra attributes
    """
    __tablename__ = 'user_groups'
    userID = db.Column('userID', saUUID, db.ForeignKey('users.userID', ondelete='CASCADE'), primary_key=True)
    groupID = db.Column('groupID', saUUID, db.ForeignKey('groups.groupID', ondelete='CASCADE'), primary_key=True)
    nickname = db.Column('nickname', db.String(32), nullable=False)
    role = db.Column('role', db.Enum(Role), nullable=False)
    group = db.relationship('DBGroup', back_populates='users')
    user = db.relationship('DBUser', back_populates='groups')

    @property
    def serialize(self) -> dict:
        """Serializes the user into a dictionary
        """
        return {
            'nickname': self.nickname,
            'role': self.role,
            'userID': self.userID
        }


class DBUser(db.Model):
    """A user table in the database
    """
    __tablename__ = 'users'
    userID = db.Column('userID', saUUID, primary_key=True)
    pubkeys = db.relationship('DBPubkey', backref='users', lazy=True)
    groups = db.relationship('DBUserGroups', backref=db.backref('users', lazy=True))

    def getGroups(self) -> List[dict]:
        """Returns a list of groups the user is a member of
        """
        return [{'groupID': group.groupID, 'groupName': group.group.groupName} for group in self.groups]


class DBPubkey(db.Model):
    """Pubkeys associated with a user
    """
    __tablename__ = 'pubkeys'
    userID = db.Column('userID', saUUID, db.ForeignKey('users.userID', ondelete='CASCADE'), primary_key=True)
    pubkey = db.Column('pubkey', db.String(512), unique=True, primary_key=True)


class DBGroup(db.Model):
    """The group table in the database
    """
    __tablename__ = 'groups'
    groupID = db.Column('groupID', saUUID, primary_key=True)
    groupName = db.Column('groupName', db.String(256), nullable=False)
    locked = db.Column('locked', db.Boolean, default=False)
    users = db.relationship('DBUserGroups', backref=db.backref('groups', lazy=True))
    expenses = db.relationship('DBExpense', backref=db.backref('groups', lazy=True))

    def toDict(self) -> dict:
        """Serializes the group into a dictionary
        """
        return {
            'groupID': self.groupID,
            'groupName': self.groupName,
            'locked': self.locked,
            'groupMembers': [user.serialize for user in self.users]
        }

    def getMemberRole(self, userID: UserID) -> Role:
        """Returns a users role in this group

        Arguments:
            userID {UserID} -- UUID of the user

        Returns:
            Role -- Permission level as enum
        """
        member = DBUserGroups.query.filter_by(userID=userID, groupID=self.groupID).first()
        return Role(member.role)

    def isAdmin(self, userID: UserID) -> bool:
        """Determine whether a user is privileged or not

        Arguments:
            userID {UserID} -- UUID of a user

        Returns:
            bool -- Wheter a user is admin or not
        """
        if self.getMemberRole(userID) == Role.ADMIN:
            return True

        return False

    def isMember(self, userID: UserID) -> bool:
        """Determines wheter a user is part of a group or not

        Arguments:
            userID {UserID} -- UUID of the user

        Returns:
            bool -- Whether a user is member or not
        """
        member = DBUserGroups.query.filter_by(userID=userID, groupID=self.groupID).first()

        if member is None:
            return False

        return True

    def promoteUser(self, userID: UserID):
        """Promotes a user to become admin
        """
        member = DBUserGroups.query.filter_by(userID=userID, groupID=self.groupID).first()

        if member is not None:
            member.role = Role.ADMIN
            db.session.add(member)
            commit()

    def getBalanceGraph(self) -> Tuple[List[List[int]], List[UserID]]:
        """Get the balance graph of the group represented as a matrix
        Columns represent the debts another user has by a user
        Rows represent the debts a user has.
        The main diagonal is always zero

        Returns:
            List[List[float]] -- The balance matrix/graph
            List[UserID] -- Association for each column/row to a user
        """
        startTime = time.time()
        balance = {}
        users = {}  # Absue dict as ordered set

        logger.debug('Constructing debt dict')

        # Generate a dict with all debts others have by a user
        for expense in self.expenses:
            logger.debug('Creator: %s', expense.creator)
            users[expense.creator] = None

            # Create entry for user if not yet existing
            balance.setdefault(expense.creator, {})

            for position in expense.positions:
                logger.debug('Defaulter: %s', position.userID)
                users[position.userID] = None

                balance[expense.creator].setdefault(position.userID, 0)
                # Calculate debts in cent
                balance[expense.creator][position.userID] += int(position.amount * 100)

        logger.debug('Balance (unmerged): %s', balance)

        graph = list()
        # Build the matrix
        for user in users:
            userBalance = balance.setdefault(user, {})
            column = list()
            for innerUser in users:
                if innerUser == user:
                    # Can't have debts with yourself
                    column.append(0)
                else:
                    column.append(userBalance.setdefault(innerUser, 0))
            graph.append(column)

        # Collect time
        total = time.time() - startTime
        RKB_GRAPH_GENERATION_LATENCY.labels(self.groupID).observe(total)
        logger.debug("Total Time: %f", total)

        return graph, list(users)


class DBExpense(db.Model):
    """Expense table
    """
    __tablename__ = 'expenses'
    expenseID = db.Column('expenseID', saUUID, primary_key=True)
    groupID = db.Column('groupID', saUUID, db.ForeignKey('groups.groupID', ondelete='CASCADE'), nullable=False)
    creator = db.Column('creator', saUUID, db.ForeignKey('users.userID', ondelete='CASCADE'), nullable=False)
    description = db.Column('description', db.Text, nullable=False)
    date = db.Column('date', db.DateTime, nullable=False)
    attachments = db.relationship('DBAttachment', backref='expenses', lazy=True)
    positions = db.relationship('DBPosition', backref='expenses', lazy=True)

    def toDict(self) -> dict:
        """Serializes the expense into a dictionary

        Returns:
            dict -- A dict of the group
        """
        return {
            'expenseID': self.expenseID,
            'groupID': self.groupID,
            'creator': self.creator,
            'description': self.description,
            'date': self.date,
            'attachments': [attachment.serialize for attachment in self.attachments],
            'positions': [position.serialize for position in self.positions]
        }

    def toShortDict(self) -> dict:
        """Returns only overview relevant data as dict

        Returns:
            dict -- dict matching shortExpense
        """
        return {
            'expenseID': self.expenseID,
            'creator': self.creator,
            'description': self.description,
            'date': self.date,
            'amount': self.getAmount(),
            'confirmed': self.getConfirmationStatus()
        }

    def getAmount(self) -> float:
        """Gets the total amount of this expense

        Returns:
            float -- The total amount
        """
        amount = db.session.query(func.sum(DBPosition.amount)).filter_by(expenseID=self.expenseID).one()
        return amount[0]

    def getConfirmationStatus(self) -> bool:
        """Returns the status of this expense (confirmed/unconfirmed)

        Returns:
            bool -- [description]
        """
        # first() returns the first row or None
        unconfirmed = DBPosition.query.filter_by(expenseID=self.expenseID, confirmed=False).first()

        if unconfirmed is None:
            return True

        return False

    def confirmPositions(self, userID: UserID):
        """Confirms all positions for the given user

        Arguments:
            userID {UserID} -- UUID of the user to confirm all positions
        """
        DBPosition.query.filter_by(expenseID=self.expenseID, userID=userID).update({'confirmed': True})
        commit()


class DBPosition(db.Model):
    """Positions table
    """
    __tablename__ = 'positions'
    positionID = db.Column('positionID', saUUID, primary_key=True)
    expenseID = db.Column('expenseID', saUUID, db.ForeignKey('expenses.expenseID', ondelete='CASCADE'), nullable=False)
    userID = db.Column('userID', saUUID, db.ForeignKey('users.userID', ondelete='CASCADE'), nullable=False)
    amount = db.Column('amount', db.Float, nullable=False)
    confirmed = db.Column('confirmed', db.Boolean, default=False)

    @property
    def serialize(self):
        """Serializes the postion as dictionary
        """
        return {
            'positionID': self.positionID,
            'expenseID': self.expenseID,
            'userID': self.userID,
            'amount': self.amount,
            'confirmed': self.confirmed
        }


class DBAttachment(db.Model):
    """Attachments table
    """
    __tablename__ = 'attachments'
    attachmentID = db.Column('attachmentID', saUUID, primary_key=True)
    expenseID = db.Column('expenseID', saUUID, db.ForeignKey('expenses.expenseID', ondelete='CASCADE'), primary_key=True)

    @property
    def serialize(self):
        """Serializes the attachment as dictionary
        """
        return {
            'attachmentID': self.attachmentID,
            'expenseID': self.expenseID
        }


class DBInvite(db.Model):
    """Invite Table
    """
    __tablename__ = 'invites'
    inviteToken = db.Column('inviteToken', saUUID, primary_key=True)
    groupID = db.Column('groupID', saUUID, db.ForeignKey('groups.groupID', ondelete='CASCADE'), nullable=False)
    expiryTime = db.Column('expiryTime', db.DateTime, nullable=False)


class DBPayment(db.Model):
    """Payments table
    """
    __tablename__ = 'payments'
    paymentID = db.Column('paymentID', saUUID, primary_key=True)
    groupID = db.Column('groupID', saUUID, db.ForeignKey('groups.groupID', ondelete='CASCADE'), nullable=False)
    amount = db.Column('amount', db.Float, nullable=False)
    recipient = db.Column('creditor', saUUID, db.ForeignKey('users.userID', ondelete='CASCADE'), nullable=False)
    debtor = db.Column('debtor', saUUID, db.ForeignKey('users.userID', ondelete='CASCADE'), nullable=False)
    sent = db.Column('sent', db.Boolean, default=False)
    received = db.Column('received', db.Boolean, default=False)

    @property
    def serialize(self):
        """Serializes the payment to the expected json
        """
        return {
            'amount': self.amount,
            'paymentID': self.paymentID,
            'recipient': self.recipient,
            'debtor': self.debtor,
            'sent': self.sent,
            'received': self.received
        }


def commit():
    """Allows to explicitly commit the current transaction
    """
    db.session.commit()
