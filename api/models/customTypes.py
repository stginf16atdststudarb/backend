#!/usr/bin/env python3
"""Custom types used within this project
"""

from typing import NewType
from enum import Enum


class Action(str, Enum):
    """Roles of a member in a group (determines permissions)
    """
    SENT = 'sent'
    REC = 'received'


# ID Typing (all UUIDs)
UserID = NewType('UserID', str)
GroupID = NewType('GroupID', str)
ExpenseID = NewType('ExpenseID', str)
AttachmentID = NewType('AttachmentID', str)
PositionID = NewType('PositionID', str)
InviteToken = NewType('InviteToken', str)
PaymentID = NewType('PaymentID', str)
