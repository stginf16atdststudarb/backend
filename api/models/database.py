#!/usr/bin/env python3
"""This module keeps all the database directed functions
"""
import logging
import uuid
from typing import List

import dateutil.parser

from api.models.customTypes import (AttachmentID, ExpenseID, GroupID,
                                    InviteToken, PositionID, UserID, PaymentID)
from api.models.databaseORM import (DBAttachment, DBExpense, DBGroup, DBInvite,
                                    DBPayment, DBPosition, DBPubkey, DBUser,
                                    DBUserGroups, commit, session)
from api.models.Roles import Role
from api.models.customTypes import Action

logger = logging.getLogger(__name__)


def createUser(userKey: str) -> UserID:
    """Creates a new user with a single pubkey

    Arguments:
        userKey {str} -- Pubkey of the user

    Returns:
        UserID -- UUID of the newly created user
    """
    user = DBUser(userID=str(uuid.uuid4()))
    user.pubkeys.append(DBPubkey(pubkey=userKey))

    session.add(user)
    commit()

    return user.userID


def getUserID(pubkey: str) -> UserID:
    """Get a user ID from a valid pubkey

    Arguments:
        pubkey {str} -- A users pubkey (a device)

    Returns:
        UserID -- The internal ID of the user
    """
    user = DBPubkey.query.filter_by(pubkey=pubkey).first()

    if user is None:
        return None

    return user.userID


def getUser(userID: UserID) -> DBUser:
    """Returns a full user object for a given ID

    Arguments:
        userID {UserID} -- UUID of the user

    Returns:
        User -- A user object
    """
    user = DBUser.query.filter_by(userID=userID).first()

    return user


def createGroup(groupName: str, userID: str, nickname: str) -> GroupID:
    """Creates a new group entry in the database

    Arguments:
        groupName {str} -- Name of the new group
        userID {str} -- UserID of the creator
        nickname {str} -- chosen nickname for this group by the creator

    Returns:
        GroupID -- UUID of the new group
    """
    group = DBGroup(groupID=str(uuid.uuid4()), groupName=groupName)
    group.users.append(DBUserGroups(nickname=nickname, userID=userID, role=Role.ADMIN))

    session.add(group)
    commit()

    return group.groupID


def getGroup(groupID: GroupID) -> DBGroup:
    """Returns a group as it is represented in the db

    Arguments:
        groupID {GroupID} -- ID of a group

    Returns:
        None -- Group doesn't exist
        DBGroup -- A group object
    """
    group = DBGroup.query.filter_by(groupID=groupID).first()

    # If group doesn't exist it will return None
    return group


def deleteGroup(groupID: GroupID):
    """Deletes a group from the database
    """
    DBGroup.query.filter_by(groupID=groupID).delete()
    commit()


def lockGroup(group: DBGroup):
    """Locks a group

    Arguments:
        group {DBGroup} -- The group object to modify
    """

    group.locked = True

    session.add(group)
    session.flush()
    commit()


def saveBalance(group: DBGroup, balance: dict):
    """Saves the calculated balance into the database

    Arguments:
        group {DBGroup} -- [description]
        balance {dict} -- [description]
    """
    for payment in balance:
        debt = DBPayment(paymentID=str(uuid.uuid4()), groupID=group.groupID, amount=payment['amount'], recipient=payment['recipient'], debtor=payment['debtor'])

        session.add(debt)
    commit()


def checkGroupMember(userID: UserID, groupID: GroupID) -> bool:
    """Confirms whether a user is part of a group

    Arguments:
        userID {UserID} -- UUI of the requesting user
        groupID {GroupID} -- UUID of the specific group

    Returns:
        bool -- User is a member or not
    """
    group = DBGroup.query.filter_by(groupID=groupID).first()

    if group is None:
        return False

    return group.isMember(userID)


def addGroupMember(groupID: GroupID, userID: UserID, nickname: str):
    """Adds a user to a group (as pedestrian)

    Arguments:
        groupID {GroupID} -- UUID of the group
        userID {UserID} -- UUID of the user
        nickname {str} -- the chosen nickname to be shown in the group
    """
    group = DBGroup.query.filter_by(groupID=groupID).first()

    group.users.append(DBUserGroups(userID=userID, nickname=nickname, role=Role.PED))
    session.add(group)
    commit()


def createNewInvite(groupID: GroupID, expiryTime) -> InviteToken:
    """Creates a new invite token associated to a group

    Arguments:
        groupID {GroupID} -- UUID of the group to create a token for

    Returns:
        InviteToken -- The magic token allowing a user to log in
    """
    token = str(uuid.uuid4())
    invite = DBInvite(inviteToken=token, groupID=groupID, expiryTime=expiryTime)

    session.add(invite)
    commit()

    return token


def getInvite(inviteToken: InviteToken) -> DBInvite:
    """Returns all data about an invite

    Arguments:
        inviteToken {InviteToken} -- The token to look for

    Returns:
        DBInvite -- The information about an invite
    """
    invite = DBInvite.query.filter_by(inviteToken=inviteToken).first()

    return invite


def createExpense(groupID, creator, description, date, positions):
    """Creates an expense and saves it to the database
    """
    expense = DBExpense(
        expenseID=str(uuid.uuid4()),
        groupID=groupID,
        creator=creator,
        description=description,
        date=dateutil.parser.parse(date)
    )

    for position in positions:
        positionID = str(uuid.uuid4())
        expense.positions.append(DBPosition(positionID=positionID, userID=position['userID'], amount=position['amount']))

    session.add(expense)
    commit()

    return expense


def getExpense(expenseID: ExpenseID) -> DBExpense:
    """Returns a requested expense from the database

    Arguments:
        expenseID {ExpenseID} -- UUID of the expense

    Returns:
        Expense -- Expense object
    """
    expense = DBExpense.query.filter_by(expenseID=expenseID).first()

    return expense


def getExpenses(groupID: GroupID) -> List[DBExpense]:
    """Returns a list of all expense objects for the requested group

    Arguments:
        groupID {groupID} -- UUID of the group to get the expenses for

    Returns:
        List[DBExpense] -- List of expense objects
    """
    return DBExpense.query.filter_by(groupID=groupID)


def deleteExpense(expenseID: ExpenseID):
    """Deletes a given expense

    Arguments:
        expenseID {ExpenseID} -- UUID of an expense to delete
    """
    DBExpense.query.filter_by(expenseID=expenseID).delete()

    commit()


def confirmExpense(userID: UserID, expenseID: ExpenseID):
    """Marks a position in an expense as confirmed

    Arguments:
        userID {UserID} -- UUID of the confirming user
        expenseID {ExpenseID} -- Expense a user wants to confirm all positions
    """
    expense = DBExpense.query.filter_by(expenseID=expenseID).first()
    expense.confirmPositions(userID)


def createAttachment(expenseID: ExpenseID, attachmentID: AttachmentID):
    """Saves the uuid of an attachment to the database

    Arguments:
        expenseID {ExpenseID} -- Expense the attachment is to be added for
        attachmentID {AttachmentID} -- UUID of the attachment (stored as this in s3)
    """
    attachment = DBAttachment(expenseID=expenseID, attachmentID=attachmentID)

    session.add(attachment)
    commit()


def updateExpense(expenseID: ExpenseID, description: str, date):
    """Updates the allowed fields of an expense

    Arguments:
        expenseID {ExpenseID} -- [description]
        description {str} -- [description]
        date {datetime} -- [description]
    """
    expense = getExpense(expenseID)

    expense.description = description
    expense.date = date

    session.add(expense)
    commit()


def deletePosition(positionID: PositionID):
    """Deletes a position from an expense

    Arguments:
        positionID {PositionID} -- UUID of an position
    """
    DBPosition.query.filter_by(positionID=positionID).delete()

    commit()


def updatePosition(positionID: PositionID, amount: float):
    """Updates the amount of a position

    Arguments:
        positionID {PositionID} -- [description]
        amount {float} -- [description]
    """
    position = DBPosition.query.filter_by(positionID=positionID).first()

    position.amount = amount
    position.confirmed = False
    session.add(position)

    commit()


def addPosition(expenseID: ExpenseID, position: dict):
    """Add a new position to an existing expense

    Arguments:
        expenseID {ExpenseID} -- [description]
        position {dict} -- [description]
    """
    expense = getExpense(expenseID)

    expense.positions.append(DBPosition(positionID=str(uuid.uuid4()), userID=position['userID'], amount=position['amount']))

    session.add(expense)
    commit()


def getPayments(groupID: GroupID) -> List[dict]:
    """Returns all payments for a group

    Arguments:
        groupID {GroupID} -- [description]

    Returns:
        List[dict] -- [description]
    """
    return DBPayment.query.filter_by(groupID=groupID)


def getPayment(paymentID: PaymentID) -> DBPayment:
    """Returns a single payment

    Arguments:
        paymentID {PaymentID} -- [description]

    Returns:
        DBPayment -- [description]
    """
    payment = DBPayment.query.filter_by(paymentID=paymentID).first()
    return payment


def markPayment(payment: DBPayment, action: Action):
    """Marks a payment as either received or updated

    Arguments:
        payment {DBPayment} -- [description]
        action {Action} -- [description]
    """
    if action == Action.REC:
        payment.received = True
    else:
        payment.sent = True

    session.add(payment)
    commit()
