#!/usr/bin/env python3
"""This module defines a generic error class
"""
from dataclasses import dataclass
from dataclasses_jsonschema import JsonSchemaMixin


@dataclass
class APIError(JsonSchemaMixin):
    """A generic error to be returned from the api
    """
    code: int
    message: str

    @classmethod
    def newError(cls, code: int, message: str):
        """Creates a new error to be returned to the user

        Arguments:
            code {int} -- An error code (usually conforming to http error codes)
            message {str} -- A descriptive error message
        """
        return cls(code=code, message=message).to_dict(), code
