#!/usr/bin/env python3
"""Functions related to colecting prometheus metrics
"""

import logging
import time

from flask import Response, request
from prometheus_client import (CONTENT_TYPE_LATEST, CollectorRegistry,
                               Histogram, generate_latest, multiprocess)

logger = logging.getLogger(__name__)

FLASK_REQUEST_LATENCY = Histogram('flask_request_latency_seconds', 'Flask Request Latency',
                                  ['method', 'endpoint', 'http_status'])


def metrics():
    registry = CollectorRegistry()
    multiprocess.MultiProcessCollector(registry)
    data = generate_latest(registry)
    return Response(data, mimetype=CONTENT_TYPE_LATEST)


def beforeRequest():
    logger.debug('Incoming request on %s', request.path)
    request.start_time = time.time()


def afterRequest(response):
    # Something not handled by this api (e.g. favicon) and we don't care about
    if request.endpoint is None:
        return response

    request_latency = time.time() - request.start_time
    endpoint = request.endpoint.replace('/api/v1.api_', '')

    FLASK_REQUEST_LATENCY.labels(request.method, endpoint, response.status_code).observe(request_latency)
    return response
