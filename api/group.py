#!/usr/bin/env python3
"""Function related to managing a group
"""

import logging
from datetime import datetime

import api.models.database as db
from api.models.APIError import APIError
from api.models.TransactionMinimizer import TransactionMinimizer

logger = logging.getLogger(__name__)


def calcBalance(group):
    """Calculates the minimized balance

    Arguments:
        group {group} -- [description]
    """
    graph, users = group.getBalanceGraph()

    tm = TransactionMinimizer(graph, users)

    return tm.minCashFlow()


def createGroup(user, body):
    """Creates a new group and returns the uuid to the user

    Arguments:
        user -- The user ID as authenticated
        body -- The request body as sent to the api

    Returns:
        string -- The uuid of the newly created group
    """
    logger.debug('User: %s tries to create new group', user)

    groupID = db.createGroup(body['groupName'], user, body["nickname"])
    logger.debug('Group %s created', groupID)

    return groupID, 200


def createInviteToken(user, groupID, body):
    """Creates a new invite token for a group in case the user is an administrator

    Arguments:
        user {userID} -- UUID of the user
        groupID {groupID} -- UUID of the group
    """
    logger.debug('User: %s tries to create an invite token for group %s', user, groupID)

    group = db.getGroup(groupID)

    if group is None or not group.isMember(user):
        return APIError.newError(404, 'Unknown group')

    if not group.isAdmin(user):
        return APIError.newError(403, 'Insufficient Privileges')

    return db.createNewInvite(group.groupID, body['expiryTime']), 200


def deleteGroup(user, groupID):
    """Deletes a group
    """
    logger.debug('User %s tries to delete group %s', user, groupID)
    group = db.getGroup(groupID)

    if group is None or not group.isMember(user):
        return APIError.newError(404, 'Invalid group ID')

    if group.isAdmin(user):
        db.deleteGroup(groupID)
        return '', 204

    return APIError.newError(403, 'Insufficient privileges')


def getGroup(user, groupID):
    """Returns the data of a requested group

    Arguments:
        user {dict} -- the user as identified by the auth function
        groupID {uuid} -- The ID of the group to retrieve

    Returns:
        dict -- A group object transformed into a dictionary
    """
    logger.debug('%s requests information about group: %s', user, groupID)
    group = db.getGroup(groupID)

    if group is None or not group.isMember(user):
        return APIError.newError(404, 'Unknown group')

    return group.toDict(), 200


def addUser(user, magicToken, body):
    """Adds a user to a group

    Arguments:
        user {[type]} -- [description]
        magicToken {[type]} -- [description]
        body {[type]} -- [description]
    """
    logger.debug('%s tries to join group with token: %s', user, magicToken)
    invite = db.getInvite(magicToken)

    if invite is None:
        return APIError.newError(404, 'Unknown token')

    if invite.expiryTime < datetime.now(invite.expiryTime.tzinfo):
        return APIError.newError(410, 'Token expired')

    db.addGroupMember(invite.groupID, user, body['nickname'])
    logger.debug('%s joined group: %s', user, invite.groupID)

    return invite.groupID, 200


def lockGroup(user, groupID):
    """Sets a group into the locked state prohibiting all changes
    """

    logger.debug('%s wants to lock group: %s', user, groupID)
    group = db.getGroup(groupID)

    if group is None or not group.isMember(user):
        return APIError.newError(404, 'No such group')

    if not group.isAdmin(user):
        return APIError.newError(403, 'Insufficient permissions')

    db.lockGroup(group)

    # Now calculate the balance
    balance = calcBalance(group)

    db.saveBalance(group, balance)
    return '', 204


def promoteUser(user, groupID, body):
    logger.debug('%s wants to promote %s in group: %s', user, body, groupID)
    group = db.getGroup(groupID)

    if not group.isAdmin(user):
        return APIError.newError(403, 'Insufficient permissions')

    group.promoteUser(body)

    return '', 204


def getBalance(user, groupID):
    """Generate the list of debts for each user
    """
    if not db.checkGroupMember(user, groupID):
        return APIError.newError(403, 'Not a member')

    logger.debug('Calculating balance for group %s', groupID)
    group = db.getGroup(groupID)

    return calcBalance(group), 200


def getReport():
    pass
