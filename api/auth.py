#!/usr/bin/env python3
"""Authentication of a user happens here
"""

import base64
import logging
import time
from hashlib import sha384
from string import Template

from connexion import request
from fastecdsa import curve, ecdsa
from fastecdsa.encoding.pem import PEMEncoder
from prometheus_client import Histogram

import api.models.database as db

logger = logging.getLogger(__name__)


RKB_AUTH_TIME = Histogram('rkb_auth_latency_seconds', 'Latency induced by the auth method',
                          ['result'])


class ValidationError(Exception):
    """Basic validation exception"""


class SignatureMissmatchException(ValidationError):
    """Exception thrown when the signature couldn't be verified"""


class KeyNotKnownException(ValidationError):
    """Exception thrown when there is no such user key"""


def validateUser(authToken: str):
    """Tries to validate a user signature

    Arguments:
        authToken {string} -- saltb64.signatureb64.pubkeyb64
    """
    startTime = time.time()
    logger.debug('Token: %s', authToken)

    try:
        salt, userSignature, userPubkey = authToken.split('.')
    except ValueError:
        logger.debug('Invalid token format')
        RKB_AUTH_TIME.labels('unkown_user').observe(time.time() - startTime)

        return None

    logger.debug('salt: %s', salt)
    logger.debug('userSignature: %s', userSignature)
    logger.debug('userPubkey: %s', userPubkey)

    userID = db.getUserID(userPubkey)

    if userID is None:
        logger.debug('Unknown user')
        RKB_AUTH_TIME.labels('unkown_user').observe(time.time() - startTime)

        # Reject unknown key
        return None

    logger.debug('Identified user ID: %s', userID)
    pemHeader = '-----BEGIN PUBLIC KEY-----'
    pemFooter = '-----END PUBLIC KEY-----'
    pemKey = Template('$header\n$pubkey\n$footer').substitute(header=pemHeader, footer=pemFooter, pubkey=userPubkey)
    pubKey = PEMEncoder.decode_public_key(pemKey, curve=curve.P384)

    sig = bytearray(base64.b64decode(userSignature))
    r = int.from_bytes(sig[:48], byteorder='big', signed=False)
    s = int.from_bytes(sig[48:], byteorder='big', signed=False)
    logger.debug('r: %d', r)
    logger.debug('s: %d', s)
    logger.debug('Request: %s', request.data)

    message = base64.b64decode(salt) + request.data
    logger.debug('Message to verify: %s', message)
    if ecdsa.verify((r, s), message, pubKey, hashfunc=sha384, curve=curve.P384):
        logger.debug('Successfully verified user %s', userID)
        RKB_AUTH_TIME.labels('success').observe(time.time() - startTime)

        return {'uid': userID}

    logger.debug('verification failed')

    RKB_AUTH_TIME.labels('verification_failed').observe(time.time() - startTime)

    return None
