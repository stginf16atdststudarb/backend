#!/usr/bin/env python3
"""Functions related to managing expenses
"""

import logging

import dateutil

import api.models.database as db
from api.models.APIError import APIError

logger = logging.getLogger(__name__)


def addExpense(user, groupID, body):
    """Add a new expense to a group
    """
    logger.debug('User: %s tries to create a new expense in group %s', user, groupID)

    if not db.checkGroupMember(user, groupID):
        logger.debug('User: %s is not part of group %s', user, groupID)
        return APIError.newError(403, 'Not a Member')

    expense = db.createExpense(groupID, user, body['description'], body['date'], body['positions'])

    logger.debug('Created new expense %s', expense.expenseID)

    return expense.expenseID, 200


def confirmExpense(user, groupID, expenseID):
    """Allows a user to confirm an expense
    """
    logger.debug('User: %s tries to confirm expense %s in group %s', user, expenseID, groupID)

    if not db.checkGroupMember(user, groupID):
        logger.debug('User: %s is not part of group %s', user, groupID)
        return APIError.newError(404, 'Not a Member')

    db.confirmExpense(user, expenseID)

    return '', 204


def deleteExpense(user, groupID, expenseID):
    """Delete an expense
    """
    if not db.checkGroupMember(user, groupID):
        logger.debug('User: %s is not part of group %s', user, groupID)
        return APIError.newError(403, 'Not a Member')

    expense = db.getExpense(expenseID)
    group = db.getGroup(groupID)

    if not (expense.creator == user) or not group.isAdmin(user):
        logger.debug('User %s tried to delete expense %s with insufficient privileges', user, expenseID)
        return APIError.newError(403, 'Not admin or creator')

    db.deleteExpense(expenseID)

    return '', 204


def getExpense(user, groupID, expenseID):
    """Get all data for a specific expense
    """
    if not db.checkGroupMember(user, groupID):
        logger.debug('User: %s is not part of group %s', user, groupID)
        return APIError.newError(403, 'Not a Member')

    expense = db.getExpense(expenseID)
    if expense is None:
        # Invalid ID
        return APIError.newError(404, 'Expense ID not found')

    return expense.toDict(), 200


def getExpenses(user, groupID):
    """Get a list of all expenses for a group
    """
    if not db.checkGroupMember(user, groupID):
        logger.debug('User: %s is not part of group %s', user, groupID)
        return APIError.newError(403, 'Not a Member')
    expenseList = db.getExpenses(groupID)
    shortExpenses = list()
    for expense in expenseList:
        shortExpenses.append(expense.toShortDict())

    return shortExpenses, 200


def updateExpense(user, groupID, expenseID, body):
    """Update the data of a specific expense
    """
    expense = db.getExpense(expenseID)
    group = db.getGroup(groupID)

    if group is None or expense is None:
        return APIError.newError(404, 'Not found')

    if not (expense.creator == user) or not group.isAdmin(user):
        logger.debug('User %s tried to delete expense %s with insufficient privileges', user, expenseID)
        return APIError.newError(403, 'Not admin or creator')

    db.updateExpense(expenseID, body['description'], dateutil.parser.parse(body['date']))

    return '', 204


def addPosition(user, groupID, expenseID, body):
    """Adds a position to an expense
    """
    group = db.getGroup(groupID)
    expense = db.getExpense(expenseID)

    if group is None or expense is None:
        return APIError.newError(404, 'Not found')

    if not (expense.creator == user) or not group.isAdmin(user):
        logger.debug('User %s tried to delete expense %s with insufficient privileges', user, expenseID)
        return APIError.newError(403, 'Not admin or creator')

    db.addPosition(expenseID, body)

    return '', 204


def updatePosition(user, groupID, expenseID, positionID, body):
    """Update data of a specific position
    """
    group = db.getGroup(groupID)
    expense = db.getExpense(expenseID)

    if group is None or expense is None:
        return APIError.newError(404, 'Not found')

    if not (expense.creator == user) or not group.isAdmin(user):
        logger.debug('User %s tried to delete expense %s with insufficient privileges', user, expenseID)
        return APIError.newError(403, 'Not admin or creator')

    db.updatePosition(positionID, body['amount'])

    return '', 204


def deletePosition(user, groupID, expenseID, positionID):
    """Delete a position from an expense
    """
    group = db.getGroup(groupID)
    expense = db.getExpense(expenseID)

    if group is None or expense is None:
        return APIError.newError(404, 'Not found')

    if not (expense.creator == user) or not group.isAdmin(user):
        logger.debug('User %s tried to delete expense %s with insufficient privileges', user, expenseID)
        return APIError.newError(403, 'Not admin or creator')

    db.deletePosition(positionID)

    return '', 204
