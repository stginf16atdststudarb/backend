openapi: "3.0.0"
info:
  description: "This is a first mock of an api and anything but stable"
  version: "0.1.0"
  title: "stginf16atdststudarbe"
  termsOfService: "http://swagger.io/terms/"
  contact:
    email: "stginf16atdststudarb@scimeda.de"
  license:
    name: "GPLv3"
    url: "https://www.gnu.org/licenses/gpl-3.0.en.html"

servers:
  - url: /api/v1
    description: "The APIs base path"

tags:
- name: "users"
  description: "Managing users"
- name: "groups"
  description: "Managing groups"
- name: "expenses"
  description: "Managing expenses"
- name: "positions"
  description: "Modifying positions of an expense"
- name: "receipts"
  description: "Attachments to an expense"
- name: "payments"
  description: "Payments between members of a group"
- name: "metrics"
  description: "Metrics about this application"

paths:
  /user:
    get:
      tags:
      - "users"
      summary: "Get a users id"
      description: ""
      operationId: "api.user.getUserID"
      responses:
        200:
          description: "Success"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/userID"

  /user/register:
    post:
      tags:
      - "users"
      summary: "Register a new key aka user"
      description: ""
      operationId: "api.user.register"
      requestBody:
        description: "A new key"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/userKey"
      responses:
        406:
          description: "Invalid input"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Success"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/userID"
      security: []

  /user/groups:
    get:
      tags:
      - "users"
      summary: "Returns all groups a user is part of"
      description: ""
      operationId: "api.user.getUserGroups"
      responses:
        404:
          description: "User not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Array of groups"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/groupArray"

  /user/push:
    post:
      tags:
      - "users"
      summary: "Registers a device of a user to the push notification service"
      description: ""
      operationId: "api.user.registerPush"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Success"

  /group:
    post:
      tags:
      - "groups"
      summary: "Creates a new group"
      description: "backend creates group, returns uuid (group key), associates key/user with group, gives admin permissions"
      operationId: "api.group.createGroup"
      requestBody:
        description: "Group Data"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/newGroupData"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Group created"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/groupID"

  /group/{groupID}:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
    get:
      tags:
      - "groups"
      summary: "Get all data about a group"
      description: ""
      operationId: "api.group.getGroup"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Unknown groupID"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Group data"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/groupData"
    delete:
      tags:
      - "groups"
      summary: "Delete a group"
      description: ""
      operationId: "api.group.deleteGroup"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        403:
          description: "Insufficient privileges"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Invalid group ID"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        204:
          description: "Success"

  /group/{groupID}/member/promote:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
    post:
      tags:
      - "groups"
      summary: "Allows to promote another user to group admin"
      description: ""
      operationId: "api.group.promoteUser"
      requestBody:
        description: "User ID"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/userID"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        403:
          description: "Insufficient permissions"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        204:
          description: "Success"

  /group/{groupID}/expense:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
    get:
      tags:
      - "expenses"
      summary: "Returns an overview of expenses for a group"
      description: ""
      operationId: "api.expense.getExpenses"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Group ID not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Expense Overview"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/expenseOverview"
    post:
      tags:
      - "expenses"
      summary: "Add a new expense"
      description: ""
      operationId: "api.expense.addExpense"
      requestBody:
        description: "Expense data"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/newExpense"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Group ID not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Successfully added expense"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/expenseID"

  /group/{groupID}/expense/{expenseID}:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
      - in: "path"
        name: "expenseID"
        required: true
        description: "The id of the expense"
        schema:
          $ref: "#/components/schemas/expenseID"
    get:
      tags:
      - "expenses"
      summary: "Returns data for a specific expense"
      description: ""
      operationId: "api.expense.getExpense"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Group ID not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Expense Data"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/expenseData"
    put:
      tags:
      - "expenses"
      summary: "Updates a specific expense"
      description: ""
      operationId: "api.expense.updateExpense"
      requestBody:
        description: "Expense data"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/updateExpense"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Group ID not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        204:
          description: "Successfully edited"
    delete:
      tags:
      - "expenses"
      summary: "Delete a specific expense"
      description: ""
      operationId: "api.expense.deleteExpense"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        403:
          description: "Insufficient privileges"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        204:
          description: "Success"

  /group/{groupID}/expense/{expenseID}/confirm:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
      - in: "path"
        name: "expenseID"
        required: true
        description: "The id of the expense"
        schema:
          $ref: "#/components/schemas/expenseID"
    post:
      tags:
      - "expenses"
      summary: "Confirms an expense"
      description: ""
      operationId: "api.expense.confirmExpense"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        403:
          description: "Not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        204:
          description: "Successfully confirmed"

  /group/{groupID}/expense/{expenseID}/receipt:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
      - in: "path"
        name: "expenseID"
        required: true
        description: "The id of the expense"
        schema:
          $ref: "#/components/schemas/expenseID"
    post:
      tags:
      - "receipts"
      summary: "Adds a new receipt to an expense"
      description: ""
      operationId: "api.receipt.uploadReceipt"
      requestBody:
        description: "Receipt image"
        content:
          image/png:
            schema:
              type: "string"
              format: "binary"
          image/jpeg:
            schema:
              type: "string"
              format: "binary"
          application/pdf:
            schema:
              type: "string"
              format: "binary"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unauthorized"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Successfully uploaded receipt"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/receiptID"

  /group/{groupID}/expense/{expenseID}/receipt/{receiptID}:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
      - in: "path"
        name: "expenseID"
        required: true
        description: "The id of the expense"
        schema:
          $ref: "#/components/schemas/expenseID"
      - in: "path"
        name: "receiptID"
        required: true
        schema:
          $ref: "#/components/schemas/receiptID"
    get:
      tags:
      - "receipts"
      summary: "Obtain a receipt"
      description: ""
      operationId: "api.receipt.getReceipt"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unauthorized"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Unknown receipt id"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Receipt data"
          content:
            image/png:
              schema:
                type: "string"
                format: "binary"
            image/jpeg:
              schema:
                type: "string"
                format: "binary"
            application/pdf:
              schema:
                type: "string"
                format: "binary"

  /group/{groupID}/expense/{expenseID}/position:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
      - in: "path"
        name: "expenseID"
        required: true
        description: "The id of the expense"
        schema:
          $ref: "#/components/schemas/expenseID"
    post:
      tags:
      - "positions"
      summary: "Adss a position to an existing expense"
      description: ""
      operationId: "api.expense.addPosition"
      requestBody:
        description: "Position data"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/newPosition"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Group ID not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        204:
          description: "Successfully created"

  /group/{groupID}/expense/{expenseID}/position/{positionID}:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
      - in: "path"
        name: "expenseID"
        required: true
        description: "The id of the expense"
        schema:
          $ref: "#/components/schemas/expenseID"
      - in: "path"
        name: "positionID"
        required: true
        description: "The id of the position"
        schema:
          $ref: "#/components/schemas/positionID"
    put:
      tags:
      - "positions"
      summary: "Updates a specific position"
      description: ""
      operationId: "api.expense.updatePosition"
      requestBody:
        description: "Position data"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/newPosition"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Group ID not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        204:
          description: "Successfully edited"
    delete:
      tags:
      - "positions"
      summary: "Delete a specific position"
      description: ""
      operationId: "api.expense.deletePosition"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Group ID not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        403:
          description: "Insufficient privileges"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        204:
          description: "Success"

  /group/{groupID}/payment:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
    get:
      tags:
      - "payments"
      summary: "Returns an overview of payments for a group"
      description: ""
      operationId: "api.payment.getPayments"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Group ID not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        409:
          description: "Group is not locked"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Expense Overview"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/payments"

  /group/{groupID}/payment/{paymentID}/{action}:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
      - in: "path"
        name: "paymentID"
        required: true
        description: "The id of the expense"
        schema:
          $ref: "#/components/schemas/paymentID"
      - in: "path"
        name: "action"
        required: true
        description: "Either that the payment has been sent or received"
        schema:
          type: "string"
          enum:
            - sent
            - received
    post:
      tags:
      - "payments"
      summary: "Sets a payment to either be sent or received"
      description: ""
      operationId: "api.payment.updatePayment"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Group ID not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        403:
          description: "Editing a payment not being a member of"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        204:
          description: "Successfully edited"

  /group/{groupID}/balance:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
    get:
      tags:
      - "groups"
      summary: "Get the balance for a group showing who has to pay or receive what"
      description: ""
      operationId: "api.group.getBalance"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Group ID not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Balance"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/groupBalance"

  /group/{groupID}/report:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
    get:
      tags:
      - "groups"
      summary: "Returns a generated pdf report of all expenses and payments"
      description: ""
      operationId: "api.group.getReport"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Group ID not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Expense Overview"
          content:
            application/pdf:
              schema:
                type: "string"
                format: "binary"

  /group/{groupID}/lock:
    parameters:
      - in: "path"
        name: "groupID"
        required: true
        schema:
          $ref: "#/components/schemas/groupID"
        description: "The id of the group"
    post:
      tags:
      - "groups"
      summary: "Locks a group"
      description: ""
      operationId: "api.group.lockGroup"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Group ID not found"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        403:
          description: "Insufficient permissions"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        204:
          description: "Successfully locked group"

  /group/{groupID}/invite:
    post:
      tags:
      - "groups"
      summary: "Create a magic invite link to invite new members to the group"
      description: ""
      operationId: "api.group.createInviteToken"
      parameters:
        - in: "path"
          name: "groupID"
          required: true
          schema:
            $ref: "#/components/schemas/groupID"
          description: "The id of the group"
      requestBody:
        description: "Invite Link Options (e.g. expiry time)"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/inviteOptions"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Unknown group"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        401:
          description: "Unknown signature"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        403:
          description: "Insufficient Privileges"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Invite Token"
          content:
            application/json:
              schema:
                type: "string"
                format: "uuid"
                example: "2e29f964-8cf0-4ce4-b8e3-17d888a9ad24"

  /group/invite/{magicToken}:
    post:
      tags:
      - "groups"
      summary: "A user request to be added to a group"
      description: ""
      operationId: "api.group.addUser"
      parameters:
        - in: "path"
          name: "magicToken"
          required: true
          schema:
            type: "string"
      requestBody:
        description: "User join request (pubkey, username)"
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/joinData"
      responses:
        500:
          description: "Server error"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        404:
          description: "Unknown token"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        410:
          description: "Token expired"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        200:
          description: "Successfully joined group"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/groupID"

  /metrics:
    get:
      tags:
      - "metrics"
      summary: "Returns collected prometheus metrics"
      description: ""
      operationId: "api.metrics.metrics"
      responses:
        200:
          description: "OpenMetrics Data"
      security: []

components:
  securitySchemes:
    SignatureAuth:
      type: http
      scheme: bearer
      x-bearerInfoFunc: "api.auth.validateUser"

  schemas:
    userID:
      type: "string"
      format: "uuid"
      example: "0693d37c-404a-423c-b802-424341ff087b"

    userKey:
      required:
        - "userKey"
      properties:
        userKey:
          type: "string"

    groupID:
      type: "string"
      format: "uuid"
      example: "ae623b46-3297-4d5b-b5e5-3af57afb984b"

    newGroupData:
      required:
        - "groupName"
        - "nickname"
      properties:
        groupName:
          type: "string"
          example: "Holiday Trip"
        nickname:
          type: "string"
          example: "Jeanne d'Arc"

    groupArray:
      type: "array"
      items:
        required:
          - "groupID"
          - "groupName"
        properties:
          groupID:
            $ref: "#/components/schemas/groupID"
          groupName:
            type: "string"
            example: "Holiday Trip"

    groupData:
      required:
        - "groupID"
        - "groupName"
        - "groupMembers"
      properties:
        groupID:
          $ref: "#/components/schemas/groupID"
        groupName:
          type: "string"
          example: "Holiday Trip"
        groupMembers:
          type: "array"
          items:
            $ref: "#/components/schemas/groupMembers"
        locked:
            type: boolean

    groupMembers:
      required:
        - "userID"
        - "role"
        - "nickname"
      properties:
        userID:
          $ref: "#/components/schemas/userID"
        role:
          type: "string"
          enum:
            - 'admin'
            - 'pedestrian'
        nickname:
          type: "string"

    groupBalance:
      type: "array"
      items:
        $ref: "#/components/schemas/debt"

    debt:
      required:
        - "recipient"
        - "debitor"
        - "amount"
      properties:
        recipient:
           $ref: "#/components/schemas/userID"
        debitor:
          $ref: "#/components/schemas/userID"
        amount:
          type: "number"
          format: "float"
          example: "3.14"

    inviteOptions:
      properties:
        expiryTime:
          type: "string"
          format: "date-time"

    joinData:
      required:
        - "nickname"
      properties:
        nickname:
          type: "string"
          example: "Charlie Chaplin"

    expenseID:
      type: "string"
      format: "uuid"
      example: "839eb153-18c6-4e9d-b87a-8fcdda25969e"

    shortExpense:
      required:
        - expenseID
        - creator
        - description
        - date
        - amount
        - confirmed
      properties:
        expenseID:
          $ref: "#/components/schemas/expenseID"
        creator:
          $ref: "#/components/schemas/userID"
        description:
          type: "string"
          example: "Money for coffee"
        date:
          type: "string"
          format: "date-time"
        amount:
          type: "number"
          format: "float"
          example: "3.14"
        confirmed:
          type: "boolean"
          example: "false"

    expenseOverview:
      type: "array"
      items:
       $ref: "#/components/schemas/shortExpense"

    expenseData:
      required:
        - expenseID
        - groupID
        - creator
        - description
        - date
      properties:
        expenseID:
          $ref: "#/components/schemas/expenseID"
        groupID:
          $ref: "#/components/schemas/groupID"
        creator:
          $ref: "#/components/schemas/userID"
        description:
          type: "string"
          example: "Money for coffee"
        positions:
          type: "array"
          items:
            $ref: "#/components/schemas/position"
        attachments:
          type: "array"
          items:
            $ref: "#/components/schemas/receiptID"
        date:
          type: "string"
          format: "date-time"

    newExpense:
      required:
        - description
        - positions
        - date
      properties:
        description:
          type: "string"
          example: "Money for coffee"
        date:
          type: "string"
          format: "date-time"
        positions:
          type: "array"
          items:
            $ref: "#/components/schemas/newPosition"

    updateExpense:
      required:
        - description
        - date
      properties:
        description:
          type: "string"
          example: "Money for coffee"
        date:
          type: "string"
          format: "date-time"

    receiptID:
      type: "string"
      format: "uuid"
      example: "3cb172c3-000d-44f5-a917-059da42fff6d"

    positionID:
      type: "string"
      format: "uuid"
      example: "5ba19cb1-db75-49ab-92bc-ee567b8bfc9d"

    position:
      required:
        - positionID
        - expenseID
        - userID
        - amount
      properties:
        positionID:
          $ref: "#/components/schemas/positionID"
        expenseID:
          $ref: "#/components/schemas/expenseID"
        userID:
          $ref: "#/components/schemas/expenseID"
        amount:
          type: "number"
          format: "float"
          example: "3.14"
        confirmed:
          type: "boolean"
          example: "false"

    newPosition:
      required:
        - userID
        - amount
      properties:
        userID:
          $ref: "#/components/schemas/expenseID"
        amount:
          type: "number"
          format: "float"
          example: "3.14"

    paymentID:
      type: "string"
      format: "uuid"
      example: "7d66bbe5-7d22-4b5d-b0b6-eeed3509c138"

    payment:
      required:
        - "paymentID"
        - "debtor"
        - "recipient"
        - "amount"
        - "sent"
        - received
      properties:
        paymentID:
          $ref: "#/components/schemas/paymentID"
        debtor:
          type: "string"
        recipient:
          type: "string"
        amount:
          type: "integer"
          format: "int64"
        sent:
          type: "boolean"
          example: "false"
        received:
          type: "boolean"
          example: "false"

    payments:
      type: "array"
      items:
        $ref: "#/components/schemas/payment"

    error:
      required:
        - "code"
        - "message"
      properties:
        code:
          type: "integer"
        message:
          type: "string"

security:
  - SignatureAuth: []

externalDocs:
  description: "Find out more about Swagger"
  url: "http://swagger.io"
